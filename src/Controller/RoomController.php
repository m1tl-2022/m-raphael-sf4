<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Category;
use App\Entity\Room;
use App\Form\CategoryType;
use App\Form\RoomType;
use App\Form\SearchType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\RoomRepository;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class RoomController extends AbstractController
{

    private $security;

    private $roomRepository;

    public function __construct(Security $security, RoomRepository $roomRepository )
    {
        $this->security = $security;
        $this->roomRepository = $roomRepository;
    }

    /**
     * @Route("/room", name="room")
     */
    public function index(Request $request): Response
    {

        $data = new SearchData();
        $data->page = $request->get('page',1);
        $form = $this->createForm(SearchType::class,$data);
        $form->handleRequest($request);

        $rooms = $this->roomRepository->findQueryResult($data);

        if($form->get('clear')->isClicked()){
            return $this->redirectToRoute('room');
        }

        return $this->render('room/index.html.twig', [
            'controller_name' => 'RoomController',
            'rooms'=>$rooms,
            'form'=>$form->createView()

        ]);
    }

    /**
     * @Route("/show/{id}", name="room.show")
     */
    public function show($id) : Response
    {
        $room = $this->roomRepository->findOneBy(["id"=>$id]);

        return $this->render("room/show.html.twig",[
            "room"=>$room
        ]);
    }

    /**
     * @Route("/new", name="room.add", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function new(Request $request) : Response
    {

        $category = new Category();
        $formCategory = $this->createForm(CategoryType::class,$category);
        $formCategory->handleRequest($request);

        if($formCategory->isSubmitted() && $formCategory->isValid()){
          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->persist($category);
          $entityManager->flush();

          return $this->redirectToRoute("home");
        }


        $room = new Room();
        $room->setUser($this->security->getUser());
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($room);
            $entityManager->flush();

            $this->addFlash('success','La salle a bien été ajoutée.');
            return $this->redirectToRoute('room');
        }


        return $this->render('room/new.html.twig',[
            'room'=>$room,
            'form'=>$form->createView(),
            'formCategory'=>$formCategory->createView()
        ]);

    }

    /**
     * @Route("/{id}/edit",name="room.edit", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function edit(Request $request, Room $room) : Response
    {
        $form = $this->createForm(RoomType::class,$room);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $room->setUpdatedAt(new \DateTime("now"));
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success','La modification de la salle a bien été enregistrée');
            return $this->redirectToRoute('room');
        }

        return $this->render('room/edit.html.twig',[
           'room'=>$room,
           'form'=>$form->createView()
        ]);

    }

    /**
     * @Route("/{id}", name="room.delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function delete(Request $request, Room $room) : Response
    {
        if($this->isCsrfTokenValid('delete'.$room->getId(), $request->request->get('_token'))){
          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->remove($room);
          $entityManager->flush();

          $this->addFlash('success','La salle a bien été supprimée');
        }
    return $this->redirectToRoute('room');

    }


    /**
     * @Route("/room/{city}", name="room.showCity")
     * @return Response
     */
    public function showRoomsByCity($city){

        $rooms = $this->roomRepository->findByCity($city);

        return $this->render('room/showCity.html.twig',[
            'rooms'=>$rooms,
        ]);
    }
}