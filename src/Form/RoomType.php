<?php

namespace App\Form;

use App\Entity\Room;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RoomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'required'=>true,
                'attr'=>[
                    'placeholder'=>'Nom de la salle',
                    'class'=>'FormClassTest'
                ]
            ])
            ->add('capacity', IntegerType::class)
            ->add('city', TextType::class)
            ->add('description', TextareaType::class, [
                'constraints'=>[
                    new NotBlank([
                        'message'=>'Merci d\'ajouter une description'
                    ])
                ]
            ])
            ->add('imageFile',FileType::class,[
                "required"=>false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Room::class,
            'translation_domain'=>'room_form'
        ]);
    }
}
