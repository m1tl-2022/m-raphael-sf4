<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Room;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method Room|null find($id, $lockMode = null, $lockVersion = null)
 * @method Room|null findOneBy(array $criteria, array $orderBy = null)
 * @method Room[]    findAll()
 * @method Room[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomRepository extends ServiceEntityRepository
{

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Room::class);
        $this->paginator = $paginator;
    }

    // /**
    //  * @return Room[] Returns an array of Room objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Room
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAvailableRoom()
    {
        return $this->createQueryBuilder('r')
            ->where('r.isAvailable = true');
    }


    public function getSearchQuery(SearchData $search){
        $query = $this->findAvailableRoom();
        if(!empty($search->qName)){
            $query = $query
                ->andWhere('r.name LIKE :qName')
                ->setParameter('qName',"%{$search->qName}%");
        }
        if (!empty($search->qCity)){
            $query = $query
                ->andWhere('r.city LIKE :qCity')
                ->setParameter('qCity',"%{$search->qCity}%");
        }
        return $query;
    }

    public function findQueryResult(SearchData $search){
        $query = $this->getSearchQuery($search)->getQuery();

        return $this->paginator->paginate(
          $query,
          $search->page,
          10
        );
    }


    public function findByCity($city)
    {
        return $this->findAvailableRoom()
            ->andWhere('r.city LIKE :city')
            ->setParameter('city','%'.$city.'%')
            ->orderBy('r.city','DESC')
            ->getQuery()
            ->getResult();
    }

}
